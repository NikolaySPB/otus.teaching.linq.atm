﻿using Otus.Teaching.Linq.ATM.Core.Entities;
using Otus.Teaching.Linq.ATM.Core.Services;
using System;
using System.Linq;

namespace Otus.Teaching.Linq.ATM.DataAcces
{
    public class ATMService 
    {
        private int IdUser;
        private string SurNameUser;
        private string LoginAccount;
        private string PasswordAccount;
        private int IdAccount;
        private decimal CashAllAccount;

        internal void UserData(ATMManager atmManager)
        {            
            Console.Write("Введите Login: ");
            LoginAccount = Convert.ToString(Console.ReadLine());

            Console.Write("Введите Password: ");
            PasswordAccount = Convert.ToString(Console.ReadLine());
            Console.WriteLine();

            if (atmManager.Users.Any(item => item.Login == LoginAccount && item.Password == PasswordAccount))
            {
                var userData = atmManager.Users.Where(item => item.Login == LoginAccount);

                foreach (var item in userData)
                {
                    Console.WriteLine(item);
                }
            }
            else
            {
                Console.WriteLine("Данного пользователя нет в системе! Либо Login или Password введен не верно!");
            }
        }

        internal void UserDataAccount(ATMManager atmManager)
        {     
            var InputData = Convert.ToString(Console.ReadLine());
            try
            {
                var InputDataInt32 = Convert.ToInt32(InputData);

                if (InputDataInt32 == 1)
                {
                    Console.Write("Введите пользовательский Id: ");

                    IdUser = Convert.ToInt32(Console.ReadLine());

                    if (atmManager.Users.Any(item => item.Id == IdUser))
                    {
                        var userDataAccounts = atmManager.Accounts.Where(item => item.UserId == IdUser);

                        Console.WriteLine();

                        foreach (var item in userDataAccounts)
                        {
                            IdAccount = item.Id;
                            Console.WriteLine(item); // Здесь расписать каждый итем
                        }
                    }
                    else
                    {
                        Console.WriteLine("Данного пользовательского Id нет в базе данных." +
                            "\nИли пользовательский Id введен не верно.");
                    }

                }
                else if (InputDataInt32 == 2)
                {
                    Console.Write("Введите фамилию пользователя: ");

                    SurNameUser = Convert.ToString(Console.ReadLine());

                    if (atmManager.Users.Any(item => item.SurName == SurNameUser))
                    {
                        var Key = atmManager.Users.GroupBy(x => x.SurName);
                        foreach (var group in Key)
                        {
                            foreach (var item in group)
                            {
                                if (item.SurName == SurNameUser)
                                {
                                    IdUser = item.Id;
                                }
                            }
                        }

                        var userDataAccounts = atmManager.Accounts.Where(item => item.UserId == IdUser);

                        foreach (var item in userDataAccounts)
                        {
                            Console.WriteLine(item);
                        }
                    }
                    else
                    {
                        Console.WriteLine("Данного пользователя с такой фамилией нет в базе данных." +
                            "\nИли Вы ввели ее не верно.");
                    }
                }
            }
            catch
            {
                Console.WriteLine("Такого варианта инициализации нет.");
            }
        }

        internal void UserDataAccountHistory(ATMManager atmManager)
        {
            UserDataAccount(atmManager);

            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("\nИстория по каждому счёту\n");
            Console.ResetColor();

            var userDataAccounts = atmManager.Accounts.Where(item => item.UserId == IdUser);

            foreach (var itemUsearDataAccounts in userDataAccounts)
            {
                IdAccount = itemUsearDataAccounts.Id;

                var usserDataAccountHistory = atmManager.History.Where(item => item.AccountId == IdAccount);

                foreach (var item in usserDataAccountHistory)
                {
                    Console.WriteLine($"ID счета: {item.AccountId};\tID операции: {item.Id}; \tДата операции: {item.OperationDate.ToShortDateString()};" +
                        $"\tОперация: {item.OperationType};\tСумма операции: {item.CashSum} ");
                }
            }
        }

        internal void HistoryInputCashUsersData(ATMManager atmManager)
        {           
            var join = atmManager.Users.Join(atmManager.Accounts, x => x.Id, y => y.UserId, (x, y) => new
            {
                IdUser = y.UserId,
                SurName = x.SurName,
                FirstName = x.FirstName,
                MiddleName = x.MiddleName,
                Id = y.Id
            });

            var join2 = atmManager.History.Join(join, x => x.AccountId, y => y.Id, (x, y) => new
            {
                IdUser = y.IdUser,
                SurName = y.SurName,
                FirstName = y.FirstName,
                MiddleName = y.MiddleName,
                AccountId = y.Id,
                InputCash = x.OperationType,
                CashSum = x.CashSum,
                Id = x.Id
            });

            foreach (var itemHistory in join2)
            {
                if (itemHistory.InputCash == OperationType.InputCash)
                {
                    Console.WriteLine($"Id операции: {itemHistory.Id}\t\tId счета: {itemHistory.AccountId} \tСумма пополнения: {itemHistory.CashSum}" +
                        $"\tВладелец счета: {itemHistory.SurName} {itemHistory.FirstName} {itemHistory.MiddleName}");
                }
            }
        }

        internal void UserDataMoreCashSpecified(ATMManager atmManager)
        {
            var userDataMoreCashSpecified = atmManager.Accounts.Where(item => item.CashAll > CashAllAccount);

            var InputData = Convert.ToString(Console.ReadLine());

            Console.WriteLine();

            try
            {
                CashAllAccount = Convert.ToDecimal(InputData);

                if (CashAllAccount >= 0 && CashAllAccount <= atmManager.Accounts.Max(y => y.CashAll))
                {
                    foreach (var item in userDataMoreCashSpecified)
                    {
                        var userData = atmManager.Users.Where(y => y.Id == item.UserId);

                        foreach (var ud in userData)
                        {
                            Console.WriteLine($"Id счета: {item.Id};\tСумма на счете: {item.CashAll};\tВладелец счета: {ud.SurName} {ud.FirstName} {ud.MiddleName}"); 
                        }
                    }
                }
                else if (CashAllAccount > atmManager.Accounts.Max(y => y.CashAll))
                {
                    Console.WriteLine($"Нет такого счета превышающего сумму: {CashAllAccount}");
                }
                else if (CashAllAccount < 0)
                {
                    Console.WriteLine("Счет не может быть с отрицательной суммой.\nPS.: По крайней мере в данном проекте!");
                }

            }
            catch (Exception)
            {
                Console.WriteLine($"Ошибка ввода суммы N. Значение ({InputData}) не является числом, для обработки данных.");
            }
        }
    }
}
