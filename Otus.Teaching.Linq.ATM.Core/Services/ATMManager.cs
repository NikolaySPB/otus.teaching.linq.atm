﻿using Otus.Teaching.Linq.ATM.Core.Entities;
using Otus.Teaching.Linq.ATM.DataAcces;
using Otus.Teaching.Linq.ATM.DataAccess;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }

        public IEnumerable<User> Users { get; private set; }

        public IEnumerable<OperationsHistory> History { get; private set; }

        public ATMManager() { }

        private ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

        private ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();

            return new ATMManager(accounts, users, history);
        }               

        public void UserData ()
        {
            var atmManager = CreateATMManager();                       
            ATMService atmService = new ATMService();
            atmService.UserData(atmManager);
        }

        public void UserDataAccount()
        {
            var atmManager = CreateATMManager();
            ATMService atmService = new ATMService();
            atmService.UserDataAccount(atmManager);
        }

        public void UserDataAccountHistory()
        {
            var atmManager = CreateATMManager();
            ATMService atmService = new ATMService();
            atmService.UserDataAccountHistory(atmManager);
        }

        public void HistoryInputCashUsersData()
        {
            var atmManager = CreateATMManager();
            ATMService atmService = new ATMService();
            atmService.HistoryInputCashUsersData(atmManager);
        }

        public void UserDataMoreCashSpecified()
        {
            var atmManager = CreateATMManager();
            ATMService atmService = new ATMService();
            atmService.UserDataMoreCashSpecified(atmManager);
        }
    }
}