﻿using Otus.Teaching.Linq.ATM.Core.Services;
using System;

namespace Otus.Teaching.Linq.ATM.Core
{
    public class DistributionMenu
    {
        private int choice { get; set; }

        public void Сhoice()
        {
            for (; ; )
            {
                try
                {
                    choice = Convert.ToInt32(Console.ReadLine());
                }
                catch (Exception) { }

                if ((choice % 1) == 0 && choice > 0 && choice <= 9)
                {
                    choice = choice;
                    break;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Введен не существующий пункт меню");
                    Console.ResetColor();
                }
            }

            Console.ForegroundColor = ConsoleColor.DarkCyan;

            ATMManager atmManager = new ATMManager();

            switch (choice)
            {
                case 1:
                    Console.ForegroundColor = ConsoleColor.DarkCyan;
                    Console.WriteLine("\nВывод информации о заданном аккаунте по логину и паролю.\n");
                    Console.ResetColor();

                    atmManager.UserData();
                    break;
                case 2:
                    Console.ForegroundColor = ConsoleColor.DarkCyan;
                    Console.WriteLine("\nДанные о всех счетах заданного пользователя\n");
                    Console.ResetColor();

                    Console.WriteLine("Варианты инициализации пользователя:\n");
                    Console.WriteLine("\t1) По пользовательскому Id.");
                    Console.WriteLine("\t2) По фамилии пользователя.");
                    Console.Write("\nВведите номер варианта: ");

                    atmManager.UserDataAccount();
                    break;
                case 3:
                    Console.ForegroundColor = ConsoleColor.DarkCyan;
                    Console.WriteLine("\nВывод данных о всех счетах заданного пользователя, включая историю по каждому счёту.\n");
                    Console.ResetColor();

                    Console.WriteLine("Варианты инициализации пользователя:\n");
                    Console.WriteLine("\t1) По пользовательскому Id.");
                    Console.WriteLine("\t2) По фамилии пользователя.");
                    Console.Write("\nВведите номер варианта: ");

                    atmManager.UserDataAccountHistory();
                    break;
                case 4:
                    Console.ForegroundColor = ConsoleColor.DarkCyan;
                    Console.WriteLine("\nВывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта\n");
                    Console.ResetColor();

                    atmManager.HistoryInputCashUsersData();
                    break;
                case 5:
                    Console.ForegroundColor = ConsoleColor.DarkCyan;
                    Console.WriteLine("\nВывод данных о всех пользователях у которых на счёте сумма больше N.\n");
                    Console.ResetColor();

                    Console.Write("Введите сумму N: ");

                    atmManager.UserDataMoreCashSpecified();
                    break;
            }
        }
    }
}
