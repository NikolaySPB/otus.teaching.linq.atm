﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Linq.ATM.Core
{
    public class Menu
    {
        public void Board()
        {
            Console.Clear();

            List<string> listBoard = new List<string>();

            listBoard.Add("ВЫБИРИТЕ ОДИН ИЗ ВАРИАНТОВ: \n");

            listBoard.Add("1) ВЫВОД ИНФОРМАЦИИ О ЗАДАННОМ АККАУНТЕ ПО ЛОГИНУ И ПАРОЛЮ;");

            listBoard.Add("2) ВЫВОД ДАННЫХ О ВСЕХ СЧЕТАХ ЗАДАННОГО ПОЛЬЗОВАТЕЛЯ;");

            listBoard.Add("3) ВЫВОД ДАННЫХ О ВСЕХ СЧЕТАХ ЗАДАННОГО ПОЛЬЗОВАТЕЛЯ, ВКЛЮЧАЯ ИСТОРИЮ ПО КАЖДОМУ СЧЁТУ;");

            listBoard.Add("4) ВЫВОД ДАННЫХ О ВСЕХ ОПЕРАЦИЯХ ПОПОЛНЕНИЯ СЧЁТА С УКАЗАНИЕМ ВЛАДЕЛЬЦА КАЖДОГО СЧЁТА;");

            listBoard.Add("5) ВЫВОД ДАННЫХ О ВСЕХ ПОЛЬЗОВАТЕЛЯХ У КОТОРЫХ НА СЧЁТЕ СУММА БОЛЬШЕ N.");                      

            listBoard.Add("");

            foreach (var item in listBoard)
            {
                if (item == "ВЫБИРИТЕ ОДИН ИЗ ВАРИАНТОВ: \n")
                {
                    Console.ForegroundColor = ConsoleColor.Green;

                    Console.WriteLine(item);

                    Console.ResetColor();
                }
                else
                {
                    Console.WriteLine(item);
                }
            }

            DistributionMenu distributionMenu = new DistributionMenu();

            distributionMenu.Сhoice();            
        }
    }
}
