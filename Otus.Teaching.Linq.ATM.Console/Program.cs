﻿using Otus.Teaching.Linq.ATM.Core;
using System.Threading.Tasks;

namespace Otus.Teaching.Linq.ATM.Console
{
    class Program
    {
        static async Task Main(string[] args)
        {      
            Menu menu = new Menu();

            while (true)
            {
                System.Console.WriteLine("Старт приложения-банкомата...\n");
                await Task.Delay(1500);
                menu.Board();

                System.Console.ForegroundColor = System.ConsoleColor.Red;
                System.Console.WriteLine("\nДля завершение работы банкомата, нажмите клавишу (Esc).\nДля повторения приложения, нажмите любую клавишу.");
                System.Console.ResetColor();

                if (System.Console.ReadKey().Key == System.ConsoleKey.Escape)
                {
                    break;
                }

                System.Console.Clear();
            }

            System.Console.WriteLine("\nЗавершение работы приложения-банкомата... Нажмите любую клавишу.");

            System.Console.ReadLine();
        }
    }
}